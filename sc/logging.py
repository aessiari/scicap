import logging
import logging.config
import sys
import os


# TODO this should eventually go to a proper logging config section
# one issue with this approach is that env vars are passed to subprocesses,
# and so this will also affect the services started with `sc services start`
def _get_default_level():
    return os.environ.get('SC_LOG_LEVEL', logging.INFO)


def setup_logging():
    default_level = _get_default_level()

    config_dict = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'concise': {
                'format': '[%(levelname)s] %(name)s :: %(message)s'
            },
            'detailed': {
                'format': '%(asctime)s :: %(name)s :: %(levelname)s :: %(message)s'
            }
        },
        'handlers': {
            'console': {
                'level': default_level,
                'class': 'logging.StreamHandler',
                'formatter': 'concise',
            }
        },
        'loggers': {
            'sc': {
                'handlers': ['console'],
                'level': default_level,
                'propagate': True
            }
        }
    }

    logging.config.dictConfig(config_dict)


class LoggingMixin:

    def __init__(self, *args, **kwargs):
        self._logger = None
        super().__init__(*args, **kwargs)

    @property
    def log(self):
        if not self._logger:
            components = (
                self.__class__.__module__,
                self.__class__.__name__,
            )
            logger_name = str.join('.', components)
            self._logger = logging.getLogger(logger_name)
        return self._logger
